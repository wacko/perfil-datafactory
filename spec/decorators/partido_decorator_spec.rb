require 'spec_helper'

describe PartidoDecorator do
  let(:boca){Equipo.new(nombre: 'Boca')}
  let(:river){Equipo.new(nombre: 'River')}
  let(:partido){Partido.new(local: boca, visitante: river, goles_local: 1, goles_visitante: 1)}

  describe '#titulo' do
    context 'cuando el partido no se jugó' do
      it 'muestra el nombre de ambos equipos' do
        partido.estado = ""
        partido.decorate.titulo.should == 'Boca - River'
      end
    end

    context 'cuando el partido ya se jugó' do
      it 'muestra el nombre de ambos equipos y el marcador' do
        partido.estado = "finalizado"
        partido.decorate.titulo.should == 'Boca (1) - River (1)'
      end
    end
  end

  describe '#link_to_detalles' do
    it 'muestra un link a los detalles de un torneo' do
      partido = Partido.new id: 123
      partido.decorate.link_to_detalles.should == '<a href="/admin/partidos/123">detalles</a>'
    end
  end

end
