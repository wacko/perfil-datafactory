require 'spec_helper'

describe TorneoDecorator do
  let(:torneo){Torneo.new id:10}

  describe '#link_to_detalles' do
    it 'muestra un link a los detalles de un torneo' do
      torneo.decorate.link_to_detalles.should == '<a href="/admin/torneos/10">detalles</a>'
    end
  end

  describe '#link_to_fechas' do
    it 'muestra un link a las fechas de un torneo' do
      torneo.decorate.link_to_fechas.should == '<a href="/admin/torneos/10/fechas">ver fechas</a>'
    end
  end
end
