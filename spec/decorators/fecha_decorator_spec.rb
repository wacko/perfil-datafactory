require 'spec_helper'

describe FechaDecorator do
  let(:fecha){Fecha.new id:20, torneo_id: 10, numero: 4}

  describe '#link_to_detalles' do
    it 'muestra un link a los detalles de una fecha' do
      fecha.decorate.link_to_detalles.should == '<a href="/admin/torneos/10/fechas/20">detalles</a>'
    end
  end

  describe '#link_to_partidos' do
    it 'muestra un link a los partidos de una fecha' do
      fecha.decorate.link_to_partidos.should == '<a href="/admin/fechas/20/partidos">partidos</a>'
    end
  end
end
