require 'spec_helper'

describe Fecha do
  let(:xml) { Nokogiri::XML(open('./spec/xml/deportes.futbol.primeraa.fixture.xml'), nil, 'ISO-8859-1') }
  let(:fecha_xml) { xml.search('fecha').first }
  let(:torneo) { double :torneo, id: 949 }

  describe '#parse' do
    it 'inicializa los valores de Fecha' do
      fecha = subject.parse(fecha_xml, torneo)

      fecha.id.should == 20605
      fecha.torneo_id.should == 949
      fecha.estado.should == ''
      fecha.desde.to_s.should == '2011-02-11'
      fecha.hasta.to_s.should == '2011-02-14'
      fecha.numero.should == 1
    end

    it 'parsea todos los partidos' do
      partidos = double
      partidos.stub(:parse!)
      partidos.should_receive(:parse!).at_least(10).times
      subject.stub(:partidos).and_return partidos

      subject.parse(fecha_xml, torneo)
    end
  end

end
