require 'spec_helper'

describe Torneo do
  let(:xml) { Nokogiri::XML(open('./spec/xml/deportes.futbol.primeraa.fixture.xml'), nil, 'ISO-8859-1') }

  describe '#parse' do
    it 'inicializa los valores de Torneo' do
      subject.stub_chain(:fechas, :parse!)

      torneo = subject.parse xml

      torneo.id.should == 949
      torneo.categoria_id == 1
      torneo.campeonato.should == 'Torneo Clausura 2011'
    end

    it 'parsea todas las fechas' do
      fechas = double
      fechas.stub(:parse!)
      fechas.should_receive(:parse!).at_least(19).times
      subject.stub(:fechas).and_return fechas

      torneo = subject.parse xml
    end

  end
end
