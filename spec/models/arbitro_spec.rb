require 'spec_helper'

describe Arbitro do
  let(:xml) { Nokogiri::XML(open './spec/xml/partido.xml') }

  describe '#parse' do
    it 'inicializa los valores de Arbitro' do
      subject.parse(xml.search('arbitro').first)

      subject.id.should == 61950
      subject.nombre.should == "Trucco, Silvio"
      subject.nombre_corto.should == "S. Trucco"
      subject.pais.should == "Argentina"
    end
  end
end
