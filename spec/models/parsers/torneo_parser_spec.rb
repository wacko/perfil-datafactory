require 'spec_helper'

describe Parsers::TorneoParser do

  before :all do
    file = open("./spec/xml/parser_torneo.xml").read
    @xml = Nokogiri::XML(file, nil, 'ISO-8859-1')
  end

  describe "#parse_estadios" do
    it "inicializa los Estadios una sola vez" do
      Estadio.stub(:parse!)
      Estadio.should_receive(:parse!).exactly(3).times
      subject.parse_estadios @xml
    end
  end

  describe "#parse_arbitros" do
    it "inicializa los Arbitros una sola vez" do
      Arbitro.stub(:parse!)
      Arbitro.should_receive(:parse!).exactly(3).times
      subject.parse_arbitros @xml
    end
  end

  describe "#parse_equipos" do
    it "inicializa los Estadios una sola vez" do
      Equipo.stub(:parse!)
      Equipo.should_receive(:parse!).exactly(6).times
      subject.parse_equipos @xml
    end
  end

end
