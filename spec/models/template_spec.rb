require 'spec_helper'

describe Template do

  before do
    class Template
      def load_template name
        name
      end
    end
    @template = Template.instance
  end

  describe '#for' do
    it 'devuelve el template default' do
      @template.for('estadisticas', 'deportes.futbol.primeraa').should == 'estadisticas-primeraa'
      @template.for('posiciones', 'deportes.futbol.primeraa').should == 'posiciones'
    end

    it 'devuelve el template custom' do
      @template.for('fixture', 'deportes.futbol.primeraa').should == 'fixture'
      @template.for('fixture', 'deportes.futbol.libertadores').should == 'libertadores/fixture'
    end
  end

end
