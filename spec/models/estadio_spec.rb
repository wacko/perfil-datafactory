require 'spec_helper'

describe Estadio do
  let(:xml) { Nokogiri::XML(open './spec/xml/partido.xml') }

  describe '#parse' do
    it 'inicializa los valores de Estadio' do
      subject.parse(xml.search('partido').first)
      subject.id.should == 13
      subject.nombre.should == "Marcelo Bielsa"
      subject.club.should == "N.O. Boys"
      subject.ciudad.should == "Rosario"
    end
  end
end
