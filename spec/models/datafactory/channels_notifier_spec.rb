require 'spec_helper'
require_relative '../../../lib/datafactory/channels_notifier'

module Datafactory
  describe ChannelsNotifier do
    it "notifies matching observers" do
      observer = lambda {}
      observer.should_receive(:call)
      subject.add_observer /hit/, observer
      subject.notify 'hit'
    end

    it "does not notify matching observers" do
      observer = lambda {}
      subject.add_observer /hit/, observer
      observer.should_not_receive(:call)
      subject.notify 'miss'
    end

    it "pass matching channel to block" do
      arguments = nil
      observer = lambda {|canal| arguments = canal}
      subject.add_observer /hit/, observer
      subject.notify 'is a hit'
      arguments.should == 'is a hit'
    end
  end
end
