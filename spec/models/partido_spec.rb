require 'spec_helper'

describe Partido do
  let(:xml) { Nokogiri::XML(open './spec/xml/deportes.futbol.primeraa.fixture.xml') }

  describe '#parse' do
    it 'inicializa los valores de Partido' do

      partido = subject.parse(xml.search('partido').first, Fecha.new)

      partido.id.should == 103507
      partido.estadio_id.should == 15
      partido.fecha_partido.to_s.should == '2011-02-11 22:00:00 UTC'
      partido.estado.should == 'finalizado'
      partido.local_id.should == 7
      partido.visitante_id.should == 13
      partido.ganador_id.should == 7
      partido.goles_local.should == 2
      partido.goles_visitante.should == 1
      partido.arbitro_id.should == 648
      partido.medios.should == '10 79'
    end
  end

end
