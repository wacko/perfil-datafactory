require 'spec_helper'

describe Ficha do
  let(:xml) { Nokogiri::XML(open './spec/xml/deportes.futbol.primeraa.ficha.103516.xml') }
  let(:torneo) { double :torneo, id: 949 }

  describe '#parse' do
    it 'inicializa los valores de Ficha' do

      ficha = subject.parse(xml.at('fichapartido'), torneo)

      ficha.local_id.should                == 8
      ficha.visitante_id.should            == 14
      ficha.goles_local.should             == 1
      ficha.goles_penales_local.should     == nil
      ficha.goles_visitante.should         == 3
      ficha.goles_penales_visitante.should == nil

      ficha.id.should                 == 103516
      ficha.torneo_id.should          == 949
      ficha.dia.should                == '20110218'.to_date
      ficha.fecha.should              == 'Fecha 2'
      ficha.fn.should                 == 2
      ficha.horario.should            == '19:00'
      ficha.nombre_dia.should         == 'Viernes'
      ficha.nombre_nivel.should       == 'Temporada Regular'

      ficha.hora_inicio.should        == '18:58:00'
      ficha.estado_evento.should      == 2
      ficha.tiempo_evento.should      == 2
      ficha.minutos_evento.should     == 45
      ficha.segundos_evento.should    == 0
      ficha.hora_estado_evento.should == '20:50:59'
      ficha.arbitro_id.should         == 644
      ficha.estadio_id.should         == 8
    end
  end

end
