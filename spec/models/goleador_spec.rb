require 'spec_helper'

describe Goleador do
  let(:xml) { Nokogiri::XML(open './spec/xml/deportes.futbol.primeraa.goleadores.xml') }
  # let(:goleador) { Nokogiri::XML(open './spec/xml/deportes.futbol.primeraa.goleadores.xml') }

  describe '#parse' do
    it 'inicializa los valores de Goleador' do
      subject.parse(xml.search('persona').first, Fecha.new)
      subject.goles.should == 9
      subject.equipo_id.should == 136
    end
  end

  describe 'purge!' do
    it 'borra los registros de goleadores de un torneo anteriores a una determinada fecha' do
      Goleador.create persona_id: 1, torneo_id: 1, fecha: '2014-01-01'.to_date
      Goleador.create persona_id: 2, torneo_id: 1, fecha: '2014-01-02'.to_date
      Goleador.purge! 1, '2014-01-02'.to_date
      Goleador.count.should == 1
    end
  end

end
