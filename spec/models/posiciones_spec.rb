require 'spec_helper'

describe Posiciones do
  let(:xml) { Nokogiri::XML(open './spec/xml/posiciones.xml') }

  describe '#parse' do
    it 'inicializa las posiciones de un equipo' do
      posiciones = Posiciones.new torneo_id: 1872, equipo_id: 13
      posiciones.parse(xml.search('equipo').first)

      posiciones.nombre.should == "Newells"
      posiciones.puntos.should == 32
      posiciones.jugados.should == 15
      posiciones.jugados_local.should == 8
      posiciones.promedio_descenso.should == 1.436
    end
  end

end
