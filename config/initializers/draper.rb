# Issue: Upgrading to Draper 1.0 breaks ActiveAdmin
# https://github.com/gregbell/active_admin/issues/1895#issuecomment-13151276
Draper::CollectionDecorator.delegate :reorder, :page, :current_page, :total_pages, :limit_value, :total_count, :num_pages
