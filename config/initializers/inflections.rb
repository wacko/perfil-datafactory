ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.plural /(cion)$/i, '\1es'
  inflect.singular /(cion)es/i, '\1'
  inflect.plural /(dor)$/i, '\1es'
  inflect.singular /(dor)es/i, '\1'
  inflect.irregular 'incidencia', 'incidencias'
  inflect.uncountable 'posiciones'
end
