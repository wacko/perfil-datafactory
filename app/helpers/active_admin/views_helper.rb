module ActiveAdmin::ViewsHelper
  def admin_link_to resource
    return "" unless resource
    if block_given?
      link_to url_for([:admin, resource]) do
        yield
      end
    else
      link_to resource, url_for([:admin, resource])
    end
  end

  def snippet_icon resource, template
    Snippet.new(resource, template).to_icon
  end

  def snippet resource, template
    content_tag(:pre, Snippet.new(resource, template).to_text)
  end

  def gamecast_icon resource
    Gamecast.new(resource).to_icon
  end

  def gamecast resource
    content_tag(:pre, Gamecast.new(resource).to_text)
  end

end
