module ApplicationHelper
  def link_equipo equipo, texto=nil
    texto ||= equipo.to_s
    link_to texto, "http://442.perfil.com/?club=#{equipo.to_slug}"
  end

  def link_pais equipo, texto=nil
    texto ||= equipo.to_s
    "<a href='http://442.perfil.com/?pais=#{equipo.pais.parameterize}'>#{texto}</a>".html_safe
  end

  def escudo equipo
    image_tag "/escudos/#{equipo.id}.gif", alt: equipo.nombre, size:'48x47', border: 0
  end

  def escudo_small equipo
    image_tag "/escudos/#{equipo.id}.gif", alt: equipo.nombre, size:'36x36', border: 0
  end

end
