class PosicionDecorator < Draper::Decorator
  delegate_all

  def promedio_descenso
    h.number_with_precision object.promedio_descenso, precision: 3
  end

end
