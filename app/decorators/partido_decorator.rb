class PartidoDecorator < Draper::Decorator
  delegate_all

  class NullEquipo
    def initialize nombre
      @nombre = nombre.present? ? nombre : 'A definir'
    end

    def to_s
      "<i>#{@nombre}</i>".html_safe
    end

    def id
      nil
    end
  end

  def local
    object.local || NullEquipo.new(local_descripcion)
  end

  def visitante
    object.visitante || NullEquipo.new(visitante_descripcion)
  end

  def titulo
    estado == "" ? titulo_sin_goles : titulo_con_goles
  end

  def titulo_sin_goles
    "#{local} - #{visitante}"
  end

  def titulo_con_goles
    "#{local} (#{goles_local}) - #{visitante} (#{goles_visitante})"
  end

  def link_to_detalles
    h.link_to 'detalles', h.admin_partido_path(self)
  end

  def link_to_local_con_goles
    link_to_equipo_con_goles local, goles_local
  end

  def link_to_visitante_con_goles
    link_to_equipo_con_goles visitante, goles_visitante
  end

  def link_to_equipo_con_goles equipo, goles
    return "-" unless equipo.id
    if goles
      h.admin_link_to(equipo) + " (#{goles})"
    else
      h.admin_link_to(equipo)
    end
  end

  def fecha_partido
    object.fecha_partido.localtime.to_formatted_s(:short)
  end

  def escudo equipo
    h.image_tag "escudos/#{equipo.id}.gif", class: 'escudo'
  end

  def link_escudo equipo
    return "" unless equipo && equipo.id
    h.admin_link_to(equipo) do
      h.image_tag("escudos/#{equipo.id}.gif", class: 'escudo')
    end
  end

  def goles_local
    object.goles_local || '-'
  end

  def goles_visitante
    object.goles_visitante || '-'
  end

  # Imprime una lista con tags <span> por cada medio.
  # partido.medios = '10 20'
  # partido.tags_medios
  #   => <span class='canal canal-10'></span>
  #      <span class='canal canal-20'></span>
  def tags_medios
    return "" if medios.blank?
    medios.split.map { |m| "<span class='canal canal-#{m}'></span>" }.join
  end

end
