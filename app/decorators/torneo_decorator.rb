class TorneoDecorator < Draper::Decorator
  delegate_all

  def link_to_detalles
    h.link_to 'detalles', h.admin_torneo_path(self)
  end

  def link_to_estadisticas text = 'estadisticas'
    h.link_to text, h.preview_path(:torneos, self)
  end

  def link_to_fechas text = 'ver fechas'
    h.link_to text, h.admin_torneo_fechas_path(self)
  end

  def link_to_posiciones text = 'tabla de posiciones'
    h.link_to text, h.admin_torneo_posiciones_index_path(self)
  end

  def link_to_goleadores text = 'goleadores'
    h.link_to text, h.admin_torneo_goleadores_path(self)
  end

  def link_to_promedios text = 'promedios'
    h.link_to text, h.admin_torneo_promedios_path(self)
  end

  def selector_fecha fecha
    h.content_tag 'a', fecha.numero,
      data: {url: url_generator.for('fecha', fecha)},
      class: (fecha.actual? ? 'selected' : '')
  end

  def selector_nivel nivel
    h.content_tag 'a', nivel.nombre,
      data: {url: url_generator.for('nivel', nivel)},
      class: (fecha_actual.nivel == nivel ? 'selected' : '')
  end

  def url_generator
    @url_generator ||= SnippetUrl.new(model)
  end

  def link_to_partido partido
    h.link_to 'ver ficha', url_generator.ficha(partido), {target: '_blank'}
  end

  def grupos
    @grupos ||= armar_grupos
  end

private

  def fechas_fase_de_grupos
    fechas.to_a.select{|f| f.nivel == 1}.group_by(&:nombre_grupo)
  end

  def armar_grupos
    fechas_fase_de_grupos.map do |grupo, fechas|
      Grupo.new(
        grupo,
        fechas.collect(&:partidos).flatten,
        posiciones.select{|p| p.zona == grupo}
      )
    end
  end

end
