class IntegranteDecorator < Draper::Decorator
  include AssetsHandler

  delegate_all

  def goles
    h.content_tag :span, model.goles, class: 'golesJugador' if model.goles
  end

  def tarjetas
    return h.content_tag :span, model.roja, class: 'tarjetasRojasJugador' if model.roja
    h.content_tag :span, model.amarilla, class: 'tarjetasAmarillasJugador' if model.amarilla
  end

end
