class FechaDecorator < Draper::Decorator
  delegate_all

  def link_to_detalles
    h.link_to 'detalles', h.admin_torneo_fecha_path(torneo_id, self)
  end

  def link_to_partidos
    h.link_to 'partidos', h.admin_fecha_partidos_path(self)
  end

end
