class EquipoDecorator < Draper::Decorator
  delegate_all

  def escudo
    h.image_tag "escudos/#{equipo.id}.gif", class: 'escudo'
  end

  def link_con_escudo
    h.admin_link_to(equipo) do
      h.image_tag("escudos/#{equipo.id}.gif", class: 'escudo') + equipo
    end
  end

end
