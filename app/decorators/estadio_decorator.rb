class EstadioDecorator < Draper::Decorator
  delegate_all

  def nombre_y_club
    "#{nombre} (#{club})"
  end

end
