class FichaDecorator < Draper::Decorator
  delegate_all

  def goles_local
    model.goles_local || '-'
  end

  def goles_visitante
    model.goles_visitante || '-'
  end

  def local_con_goles
    "#{local} (#{goles_local})"
  end

  def visitante_con_goles
    "#{visitante} (#{goles_visitante})"
  end

  def estado
    case object.estado_evento
    when 1  then 'PT' # Primer Tiempo
    when 2  then 'Finalizado'
    when 3  then 'Suspendido'
    when 4  then 'Postergado'
    when 5  then 'Entretiempo'
    when 6  then 'ST' # Segundo Tiempo
    when 7  then 'Entretiempo alargue'
    when 8  then 'Alargue 1'
    when 9  then 'Fin alargue 1'
    when 10 then 'Alargue 2'
    when 11 then 'Fin alargue 2'
    when 12 then 'Definición por penales'
    when 49 then 'Fin segundo tiempo'
    else ""
    end
  end

  def titulo
    titulo_con_goles + estado
  end

  def titulo_con_goles
    "#{local} (#{goles_local}) - #{visitante} (#{goles_visitante})"
  end

  def marcador_dia
    object.estado_evento ? estado : nombre_dia
    # TODO: Domingo 24/11 En el día de juego se reemplaza por PT, ET o ST cuando el partido se está jugando, según corresponda
  end

  def en_juego?
    [1, 6, 8, 10].include? object.estado_evento # PT, ST, Alargue
  end

  # el horario de inicio, se reemplaza por el cronómetro cuando el partido se está jugando
  def marcador_tiempo
    case object.estado_evento
    when 0
      horario
    when 1, 6, 8, 10 # PT, ST, Alargue
      tiempo_de_juego_actual
    else ""
    end
  end

  def marquee
    incidencias.gol.map{ |gol| "Gol de #{gol.equipo}: #{gol.tiempo} #{gol.minuto}' #{gol.jugador}" }.join(' ; ')
  end

  def formacion_local
    Formacion.new integrantes.decorate.select{|i| i.equipo_id == local_id}
  end

  def formacion_visitante
    Formacion.new integrantes.decorate.select{|i| i.equipo_id == visitante_id}
  end

  def goles equipo
    incidencias.decorate.select{|x| x.tipo == 'gol' && x.equipo_id == equipo.id}
  end

  def cambios equipo
    incidencias.decorate.select{|x| x.tipo == 'cambio' && x.equipo_id == equipo.id}
  end

  def expulsados
    incidencias.decorate.select{|x| x.tipo == 'expulsado'}
  end

  def lista_expulsados
    expulsados.map(&:tiempo_jugador_equipo).join(' | ')
  end

  def cronometro
    "data-cronometro='#{marcador_tiempo}'" if en_juego?
  end

  def ver_ficha
    h.admin_link_to(self){'ver ficha'} if estado_evento > 0
  end

private

  def tiempo_de_juego_actual
    "#{minutos_evento}:#{segundos_evento}"
  end

end
