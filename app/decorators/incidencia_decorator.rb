class IncidenciaDecorator < Draper::Decorator
  delegate_all

  def jugador_gol
    case inciid
    when "10" then "#{jugador} (EC)"
    when "13" then "#{jugador} (P)"
    else jugador.to_s
    end
  end

  def minuto_tiempo
    "#{minuto}' #{tiempo}"
  end

  def tiempo_jugador_equipo
    "#{minuto_tiempo}: #{jugador} (#{equipo})"
  end

end
