class SnippetUrl

  def initialize(torneo)
    deporte = torneo.categoria.deporte
    canal = torneo.categoria.canal
    anio = torneo.created_at.year.to_s
    id = torneo.id.to_s
    @dir = File.join(deporte, canal, anio, id).to_s
  end

  def for template, model = nil
    archivo = case template
      when 'estadisticas'     then "tablas/estadisticas.html"
      when 'posiciones'       then "tablas/posiciones.html"
      when 'fixture'          then "tablas/fixture.html"
      when 'fecha'            then "fechas/#{model.id}.html"
      when 'nivel'            then "niveles/#{model.to_slug}.html"
      when 'ficha'            then "fichas/#{model.id}.html"
      when 'marcador'         then "marcadores/#{model.id}.html"
      when 'sidebar'          then "sidebar/#{model.id}.html"
      when 'goleadores'       then "tablas/goleadores.html"
      when 'promedios'        then "tablas/promedios.html"
      when 'grupo'            then "fixture/#{model.to_slug}.html"
      else ""
    end

    # /rsc/442df/futbol/primeraa/2014/2054/tablas/posiciones.html
    "#{Perfil::Application.config.cdn_redirect}/#{@dir}/#{archivo}"

  end

  def ficha partido
    ficha_url = self.for('ficha', partido)
    "/partido/?ficha=#{ficha_url}"
  end

end
