class Posiciones < ActiveRecord::Base
  belongs_to :torneo
  belongs_to :equipo

  def self.fetch! torneo, equipo
    where(torneo: torneo, equipo: equipo).first_or_create
  end

  def parse node

    xml_attributes = %W[nombre puntos jugados jugadoslocal jugadosvisitante
      ganados empatados perdidos ganadoslocal empatadoslocal perdidoslocal ganadosvisitante
      empatadosvisitante perdidosvisitante golesfavorlocal golescontralocal golesfavorvisitante
      golescontravisitante golesfavor golescontra difgol puntoslocal puntosvisitante
      puntosanterior1 jugadosanterior1 puntosanterior2 jugadosanterior2 puntosactual
      difgolactual jugadosactual puntosdescenso jugadosdescenso promediodescenso
      promediodescenso2 am rojas rojaX2am faltasPen manoPen faltasCom faltasRec faltasPenRec
      nivel nivelDesc orden ordenDesc descripcionTribDisc partidosDefTribDisc racha]

    db_attributes = %W[nombre puntos jugados jugados_local jugados_visitante
      ganados empatados perdidos ganados_local empatados_local perdidos_local ganados_visitante
      empatados_visitante perdidos_visitante goles_favor_local goles_contra_local
      goles_favor_visitante goles_contra_visitante goles_favor goles_contra diferencia_de_gol
      puntos_local puntos_visitante puntos_anterior1 jugados_anterior1 puntos_anterior2
      jugados_anterior2 puntos_actual diferencia_de_gol_actual jugados_actual puntos_descenso
      jugados_descenso promedio_descenso promedio_descenso2 amarillas rojas
      roja_doble_amonestacion faltas_penal mano_penal faltas_cometidas faltas_recibidas
      faltas_penal_recibidas nivel nivel_descripcion orden orden_descripcion
      descripcion_tribunal_disciplina partidos_def_tribunal_disciplina racha]

    self.equipo_id ||= Equipo.fetch!(self.equipo_id).id
    xml_attributes.zip(db_attributes) { |xml, db| self[db] = node.at(xml).text }
    self.zona = node['zona']
    save
    self
  end

  def self.parse! xml
    fetch!(xml['id']).parse(xml).tap{|x| x.save}
  end

end
