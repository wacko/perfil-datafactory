class Dia
  attr_reader :fecha, :nombre_dia

  def initialize(fecha, nombre_dia)
    @fecha = fecha.getlocal
    @nombre_dia = nombre_dia
  end

  def to_s
    "#{@nombre_dia} #{@fecha.getlocal.to_s(:puntos)}"
  end

  def <=> dia
    fecha <=> dia.fecha
  end

  def hash
    @fecha.to_date.hash
  end

  def eql?(other)
    @fecha.to_date == other.fecha.to_date
  end

end
