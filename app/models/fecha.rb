class Fecha < ActiveRecord::Base
  belongs_to :torneo
  has_many :partidos
  composed_of :nivel, class_name: "Nivel", mapping: [%w(nivel numero), %w(nombre_nivel nombre)]

  def self.fetch! id
    where(id: id).first_or_create
  end

  def parse node, torneo
    self.id = node['id']
    self.torneo_id = torneo.id
    self.estado = node['estado']
    self.desde = Date.parse(node['fechadesde'])
    self.hasta = Date.parse(node['fechahasta'])
    self.numero = node['fn']
    self.nivel = Nivel.new(node['nivel'], node['nombrenivel'])
    self.nombre = node['nombre']
    self.orden = node['orden']

    node.search('partido').each do |partido|
      partidos.parse! partido, self
    end

    self
  end

  def self.parse! xml, torneo
    fetch!(xml['id']).parse(xml, torneo).tap{|x| x.save}
  end

  def to_s
    "##{numero}"
  end

  def descripcion
    "Fecha ##{numero}"
  end

  def anterior?
    estado == 'anterior'
  end

  def actual?
    estado == 'actual'
  end

  def ultima?
    estado == 'ultima'
  end

  def partidos_por_dia
    partidos.group_by(&:dia).sort_by{|e| e.first}
  end

  def nombre_grupo
    nombre.split(' - ')[0]
  end

end
