class Grupo

  attr_accessor :nombre, :partidos, :posiciones

  def initialize(nombre, partidos, posiciones)
    @nombre = nombre
    @partidos = partidos
    @posiciones = posiciones
  end

  def to_s
    nombre
  end

  def to_slug
    nombre.parameterize
  end

end
