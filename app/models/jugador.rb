class Jugador < ActiveRecord::Base

  def self.fetch! id, node
    where(id: id).first_or_create do |jugador|
      jugador.nombre = node.at('nombre').text
    end
  end

  def to_s
    nombre
  end

end
