require 'nokogiri'

module Parsers
  class GoleadoresParser < Base

    def parse xml
      torneo_id = xml.at('campeonato')['id']
      @torneo = Torneo.find torneo_id
      fecha = xml.at('fechaActual').text.to_date

      goleadores = xml.css('persona')[0,10]
      goleadores.each { |goleador| Goleador.parse!(goleador, torneo_id, fecha)}
      Goleador.purge!(torneo_id, fecha)
      Goleador.where(torneo_id: torneo_id).limit(10)
    end

    def generate_html goleadores, canal
      params = {goleadores: goleadores, canal: canal}
      torneo = (goleadores.any? ? goleadores.first.torneo : Torneo.find_by_canal(canal))
      save_file("#{path_torneo(torneo)}/tablas/goleadores.html", render('goleadores', params))
    end

  end
end
