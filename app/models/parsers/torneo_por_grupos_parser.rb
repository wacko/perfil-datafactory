require 'nokogiri'

module Parsers
  class TorneoPorGruposParser < TorneoParser

    def generate_html torneo, canal
      torneo_decorado = torneo.decorate

      torneo.fechas_por_nivel.each do |nivel, fechas|
        params = {torneo: torneo_decorado, fechas: FechaDecorator.decorate_collection(fechas), canal: canal}
        save_file("#{path_torneo(torneo)}/niveles/#{nivel.to_i}.html", render('nivel', params))
      end

      if canal.match(/fixture$/)
        params = {torneo: torneo_decorado, canal: canal}
        save_file("#{path_torneo(torneo)}/tablas/estadisticas.html", render('estadisticas', params))
        save_file("#{path_torneo(torneo)}/tablas/fixture.html", render('fixture', params))
      end

    rescue Exception => e
      Datafactory.logger.error e
    end

  end
end
