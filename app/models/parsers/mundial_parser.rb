require 'nokogiri'

module Parsers
  class MundialParser < TorneoParser

    def generate_html torneo, canal
      torneo_decorado = torneo.decorate

      niveles = torneo.fechas_por_nivel
      niveles.shift
      niveles.each do |nivel, fechas|
        params = {torneo: torneo_decorado, fechas: FechaDecorator.decorate_collection(fechas), canal: canal}
        save_file("#{path_torneo(torneo)}/niveles/#{nivel.to_slug}.html", render('nivel', params))
      end

      torneo_decorado.grupos.each do |grupo|
        params = {torneo: torneo_decorado, grupo: grupo, canal: canal}
        save_file("#{path_torneo(torneo)}/fixture/#{grupo.to_slug}.html", render('grupo', params))
      end

      if canal.match(/fixture$/)
        params = {torneo: torneo_decorado, canal: canal}
        save_file("#{path_torneo(torneo)}/tablas/estadisticas.html", render('estadisticas', params))
      end

    rescue Exception => e
      Datafactory.logger.error e
    end

  end
end
