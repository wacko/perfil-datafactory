require 'nokogiri'

module Parsers
  class PosicionesParser < Base

    def parse xml
      @torneo = Torneo.fetch! xml.at('campeonato')['id']
      xml.search('equipo').map do |nodo|
        equipo = Equipo.fetch! nodo['id']
        Posiciones.fetch!(@torneo, equipo).parse nodo
      end
    end

    def generate_html posiciones, canal
      torneo = (posiciones.any? ? posiciones.first.torneo : Torneo.find_by_canal(canal))

      params = {posiciones: torneo.posiciones, promedios: PosicionesDecorator.decorate_collection(torneo.promedios), torneo: torneo, canal: canal}
      save_file("#{path_torneo(torneo)}/tablas/posiciones.html", render('posiciones', params))
      save_file("#{path_torneo(torneo)}/tablas/promedios.html", render('promedios', params))
    end

  end
end
