require 'nokogiri'

module Parsers
  class MenuParser < Base

    def generate_html torneos, canal=""
      torneos ||= Torneo.all
      dir = Rails.root.join('html', 'menu.html').to_s
      save_file(dir, render('menu', {torneos: torneos.decorate, canal: canal}))
    end

  end
end
