require 'nokogiri'

module Parsers
  class Base

    def load_template template
      Tilt::ERBTemplate.new(Rails.root.join('templates',"#{template}.erb").to_s)
    end

    def render template, params
      template = Template.for(template, params[:canal])
      Datafactory.logger.error "Template '#{template}' no existe" unless template
      template.render(self, params)
    end

    def save_file filename, contents
      File.open(filename, 'w') { |file| file.write(contents) }
      Datafactory.logger.info "HTML generado: #{filename}"
    rescue
      FileUtils.makedirs(File.dirname filename)
      Datafactory.logger.info "Directorio creado: #{File.dirname filename}"
      File.open(filename, 'w') { |file| file.write(contents) }
      Datafactory.logger.info "HTML generado: #{filename}"
    end

    # EJ: $RAILS_HOME/html/futbol/primeraa/2014/2054
    def path_torneo torneo
      deporte = torneo.categoria.deporte
      canal = torneo.categoria.canal
      anio = torneo.created_at.year.to_s
      id = torneo.id.to_s
      Rails.root.join('html', deporte, canal, anio, id).to_s
    end

    def cdn_root
      Perfil::Application.config.cdn_server
    end

    def link_equipo equipo, texto=nil
      texto ||= equipo.to_s
      "<a href='http://442.perfil.com/?club=#{equipo.to_slug}'>#{texto}</a>".html_safe
    end

    def link_pais equipo, texto=nil
      texto ||= equipo.to_s
      "<a href='http://442.perfil.com/?pais=#{equipo.pais.parameterize}'>#{texto}</a>".html_safe
    end

    def escudo equipo
      return '' unless equipo and equipo.id
      "<img src='#{cdn_root}/escudos/#{equipo.id}.gif' height='48' width='47' alt='#{equipo}'>".html_safe
    end

    def escudo_small equipo
      return '' unless equipo and equipo.id
      "<img src='#{cdn_root}/escudos/#{equipo.id}.gif' height='36' width='36' alt='#{equipo}'>".html_safe
    end

    def canal_tag canal
      canales = canal.split('.').map{|a| "df_#{a}"}.join ' '
      "<div class='#{canales}'>".html_safe
    end

  end
end
