module Parsers
  class PosicionesPorGrupoParser < PosicionesParser

    def generate_html posiciones, canal
      torneo = posiciones.first.torneo
      params = {posiciones: torneo.posiciones, promedios: PosicionesDecorator.decorate_collection(torneo.promedios), torneo: torneo, canal: canal}
      save_file("#{path_torneo(torneo)}/tablas/posiciones.html", render('posiciones', params))
    end

  end
end
