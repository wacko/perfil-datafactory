require 'nokogiri'

module Parsers
  class TorneoParser < Base

    def parse xml
      parse_equipos xml
      parse_estadios xml
      parse_arbitros xml
      parse_torneo xml
    end

    def parse_torneo xml
      id = xml.at('campeonato')['id']
      torneo = Torneo.fetch!(id).parse(xml)
    end

    def parse_equipos xml
      xml.css('partido local, partido visitante').to_a.
        uniq {|equipo| equipo['id']}.
        select {|equipo| equipo['id'].present?}.
        each {|equipo| Equipo.parse!(equipo)}
    end

    def parse_estadios xml
      xml.search('partido').to_a.
        uniq {|estadio| estadio['idEstadio']}.
        select {|estadio| estadio['idEstadio'].present?}.
        each {|estadio| Estadio.parse!(estadio) }
    end

    def parse_arbitros xml
      xml.search('arbitro').to_a.
        uniq {|arbitro| arbitro['id']}.
        select {|arbitro| arbitro['id'].present?}.
        each {|arbitro| Arbitro.parse!(arbitro) }
    end

    def generate_html torneo, canal
      torneo_decorado = torneo.decorate
      if m = canal.match(/fecha.(actual|proxima|ultima)/)
        fecha = torneo.fechas.where(estado: m[1]).first
        params = {torneo: torneo_decorado, fecha: fecha.decorate, canal: canal}
        save_file("#{path_torneo(torneo)}/fechas/#{fecha.id}.html", render('fecha', params))
      end

      if m = canal.match(/fixture/)
        torneo.fechas.each do |fecha|
          params = {torneo: torneo_decorado, fecha: fecha.decorate, canal: canal}
          save_file("#{path_torneo(torneo)}/fechas/#{fecha.id}.html", render('fecha', params))
        end
        params = {torneo: torneo_decorado, canal: canal}
        save_file("#{path_torneo(torneo)}/tablas/estadisticas.html", render('estadisticas', params))
        save_file("#{path_torneo(torneo)}/tablas/fixture.html", render('fixture', params))
      end
    end

  end
end
