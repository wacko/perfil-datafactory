require 'nokogiri'

module Parsers
  class FichaParser < Base

    def parse xml
      node = xml.at('fichapartido')
      torneo = Torneo.find xml.at('campeonato')['id']
      ficha = Ficha.parse! node, torneo
    end

    def generate_html ficha, canal
      partido = ficha.partido
      fecha = partido.fecha
      torneo = fecha.torneo
      params = {ficha: ficha.decorate, partido: partido.decorate, fecha: fecha.decorate, torneo: torneo.decorate, canal: canal}

      save_file("#{path_torneo(torneo)}/marcadores/#{ficha.id}.html", render('marcador', params))
      save_file("#{path_torneo(torneo)}/sidebar/#{ficha.id}.html", render('sidebar', params))
      save_file("#{path_torneo(torneo)}/fichas/#{ficha.id}.html", render('ficha', params))
    end

  end
end
