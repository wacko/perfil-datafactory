class Equipo < ActiveRecord::Base

  def self.fetch! id
    where(id: id).first_or_create
  end

  def parse node
    self.id = node['id']
    self.nombre = node.text
    self.nombre_asociacion = node['nombreasociacion']
    self.pais = node['pais']
    self
  end

  def self.parse! xml
    return if xml['id'].blank? # Las fechas de las etapas eliminatorias inicialmente no tienen un Equipo definido
    fetch!(xml['id']).parse(xml).tap{|x| x.save}
  end

  def to_s
    nombre
  end

  def to_slug
    slug.present? ? slug : nombre.gsub("`", "").parameterize # Fix: Newell`s => Newells
  end

end
