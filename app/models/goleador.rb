class Goleador < ActiveRecord::Base
  belongs_to :torneo
  belongs_to :equipo
  default_scope { order(goles: :desc) }

  def self.fetch! id, torneo_id
    where(persona_id: id, torneo_id: torneo_id).first_or_initialize
  end

  def parse node, fecha
    self.fecha = fecha
    self.nombre = node.at('nombre').text
    self.goles = node.at('goles').text
    self.jugada = node.at('jugada').text
    self.cabeza = node.at('cabeza').text
    self.tiro_libre = node.at('tirolibre').text
    self.penal = node.at('penal').text
    self.equipo_id = node.at('equipo')['id']
    self.pais_id = node.at('pais')['id']
    self
  end

  def self.parse! node, torneo_id, fecha
    fetch!(node['id'], torneo_id).parse(node, fecha).tap{|x| x.save}
  end

  def self.purge! torneo_id, fecha
    where('torneo_id = ? AND fecha < ?', torneo_id, fecha.to_date).delete_all
  end

  def to_s
    "#{nombre} (#{goles})"
  end

end
