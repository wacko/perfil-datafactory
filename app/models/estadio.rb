class Estadio < ActiveRecord::Base

  def self.fetch! id
    where(id: id).first_or_create
  end

  # node == Partido
  def parse node
    self.id = node['idEstadio']
    self.nombre = node['nombreEstadio']
    self.club = node['clubEstadio']
    self.ciudad = node['lugarCiudad']
    self
  end

  def self.parse! xml
    fetch!(xml['idEstadio']).parse(xml).tap{|x| x.save}
  end

  def to_s
    club
  end

end
