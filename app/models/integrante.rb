class Integrante < ActiveRecord::Base
  belongs_to :jugador
  belongs_to :ficha
  belongs_to :equipo
  scope :equipo, ->(equipo_id) { where(equipo_id: equipo_id) }
  scope :titular, ->{ where(tipo: 'Titular') }
  scope :suplente, ->{ where(tipo: 'Suplente') }
  scope :dt, ->{ where(rol: 'DT') }

  def self.fetch! jugador_id, ficha_id
    where(jugador_id: jugador_id, ficha_id: ficha_id).first_or_create
  end

  def parse integrante, ficha_id, equipo_id
    self.jugador = Jugador.fetch!(integrante['id'], integrante)
    self.ficha_id = ficha_id
    self.equipo_id = equipo_id
    self.camiseta = integrante.at('camiseta').try :text
    self.goles = integrante.at('goles').try(:[], 'cant')
    self.amarilla = integrante.at('amarilla').try(:[], 'cant')
    self.roja = integrante.at('roja').try(:[], 'cant')
    self.idrol = integrante['idrol']
    self.orden = integrante['orden']
    self.rol = integrante['rol']
    self.tipo = integrante['tipo']

    self
  end

  def self.parse! integrante, ficha_id, equipo_id
    fetch!(integrante['id'], ficha_id).parse(integrante, ficha_id, equipo_id).tap{|x| x.save}
  end

  def nombre
    jugador.nombre
  end

  def tarjeta?
    roja || amarilla
  end

  def tarjeta
    return 'roja' if roja
    return 'amarilla' if amarilla
    ""
  end

  def to_s
    jugador.to_s
  end

  def titular?
    tipo == 'Titular'
  end

  def suplente?
    tipo == 'Suplente'
  end

  def dt?
    rol == 'DT'
  end

end
