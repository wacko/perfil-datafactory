class Snippet

  def initialize(model, template)
    @model = model
    @template = template
  end

  def id
    "df-#{@template}-#{@model.id}"
  end

  def path
    url_generator.for @template, @model
  end

  def url_generator
    return @url_generator if @url_generator
    torneo = (@model.respond_to?(:torneo) ? @model.torneo : @model)
    @url_generator ||= SnippetUrl.new(torneo)
  end

  # <div class="df-torneo-2054"></div>
  # <script type="text/javascript">
  #   $(document).ready(function(){
  #     traer_datafactory('df-torneo-2054', 'http://442.perfil.com/rsc/442df/futbol/primeraa/2014/2054/tablas/posiciones.html');
  #   });
  # </script>
  def to_s
    # Tiempo de regarga de los marcadores (en milisegundos)
    timeout = recurrente? ? 120_000 : 'undefined'

    code = <<-SNIPPET
<div class="df_widget #{id}"></div>
<script type="text/javascript">
  $(document).ready(function(){
    traer_datafactory('.#{id}', '#{path}', #{timeout});
  });
</script>
    SNIPPET
  end

  # <span class="copy-button" data-clipboard-text="[[code]]" title="Click to copy me">Copy to Clipboard</span>
  def to_text
    ActionController::Base.helpers.content_tag(:span, self.to_s, attributes)
  end

  def to_icon
    ActionController::Base.helpers.image_tag('clipboard.png', attributes)
  end

private

  def attributes
    {
      'id' => "copy-button",
      'class' => "copy-button",
      'data-clipboard-text' => to_s,
      'title' => "Copiar #{@template}"
    }
  end

  # Snippets que se recargan periodicamente
  def recurrente?
    %w[ficha marcador sidebar].include? @template
  end

end
