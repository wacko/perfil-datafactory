class Nivel

  include Comparable

  attr_reader :numero, :nombre

  def initialize(numero, nombre)
    @numero, @nombre = numero, nombre
  end

  def <=>(otro)
    numero <=> otro.numero
  end

  def ==(otro)
    if otro.respond_to? :numero
      numero == otro.numero
    else
      numero == otro
    end
  end

  def hash
    @numero.hash
  end

  def eql?(otro)
    hash == otro.hash
  end

  def to_s
    nombre
  end

  def to_slug
    nombre.parameterize
  end

  def to_i
    numero
  end

end
