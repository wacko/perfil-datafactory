class Partido < ActiveRecord::Base
  belongs_to :fecha
  belongs_to :torneo
  belongs_to :local, class_name: 'Equipo'
  belongs_to :visitante, class_name: 'Equipo'
  belongs_to :ganador, class_name: 'Equipo'
  belongs_to :estadio
  belongs_to :arbitro
  has_one :ficha, foreign_key: :id

  composed_of :dia, mapping: [%w(fecha_partido fecha), %w(nombre_dia nombre_dia)]

  def self.fetch! id
    where(id: id).first_or_create
  end

  def parse node, fecha
    self.id = node['id']
    self.fecha = fecha
    self.torneo_id = fecha.torneo_id
    self.fecha_partido = Time.parse "#{node['fecha']} #{node['hora']}"
    self.estado = node.at('estado').text.downcase
    self.local_id = node.at('local')['id']
    self.visitante_id = node.at('visitante')['id']
    self.local_descripcion = node.at('local').text unless self.local_id.present?
    self.visitante_descripcion = node.at('visitante').text unless self.visitante_id.present?
    self.ganador_id = node['idGan']
    self.goles_local = node.at('goleslocal').text
    self.goles_visitante = node.at('golesvisitante').text
    self.arbitro_id = node.at('arbitro')['id'] if node.at('arbitro')
    self.estadio_id = node['idEstadio']
    self.nombre_dia = node['nombreDia']
    self.numero = node['nro']
    self.orden_agenda = node['ordenAgenda']
    self.medios = node.css('medio').map {|m| m['id'] }.join(' ')

    self
  end

  def self.parse! node, fecha=nil
    fetch!(node['id']).parse(node, fecha).tap{|x| x.save}
  end

  def iniciado?
    estado.present?
  end

  def ganador_local?
    ganador_id.present? && (ganador_id == local_id)
  end

  def ganador_visitante?
    ganador_id.present? && (ganador_id == visitante_id)
  end

end
