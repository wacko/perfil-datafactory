class Torneo < ActiveRecord::Base
  belongs_to :categoria
  has_many :fechas
  has_many :posiciones, -> { order(puntos: :desc) }
  has_many :goleadores, -> { order(goles: :desc) }
  has_many :fichas
  has_many :partidos

  delegate :canal, :deporte, to: :categoria

  def self.fetch! id
    where(id: id).first_or_create
  end

  def self.find_by_canal canal
    canal = canal.split('.') || canal
    categoria = Categoria.find_by_canal canal
    where(categoria: categoria).last
  end

  def parse node
    self.id = node.at('campeonato')['id']
    self.categoria_id = node.at('categoria')['id']
    self.campeonato = node.at('campeonato').text

    self.save
    node.search('fecha').each do |fecha|
      fechas.parse! fecha, self
    end

    self
  end

  def self.parse! xml
    id = xml.at('campeonato')['id']
    fetch!(id).parse(xml).tap{|x| x.save}
  end

  def fecha_anterior
    fechas.select(&:anterior?).first
  end

  def fecha_actual
    fechas.select(&:actual?).first
  end

  def fecha_ultima
    fechas.select(&:ultima?).first
  end

  def to_s
    campeonato
  end

  def promedios
    posiciones.sort_by{|x| x.promedio_descenso.to_f}.reverse
  end

  def niveles
    fechas.map(&:nivel).uniq
  end

  def fechas_por_nivel
    fechas.group_by(&:nivel)
  end

  def nivel_actual
    fecha_actual.nivel
  end

end
