class Template

  include Singleton

  def for name, canal = nil
    custom(name, canal) or default(name)
  end

  def self.for name, canal = nil
    instance.for name, canal
  end

private

  def initialize
    @default = {
      'estadisticas' => load_template('estadisticas-primeraa'),
      'posiciones'   => load_template('posiciones'),
      'fixture'      => load_template('fixture'),
      'fecha'        => load_template('fecha'),
      'ficha'        => load_template('ficha'),
      'marcador'     => load_template('marcador'),
      'sidebar'      => load_template('marcador-sidebar'),
      'goleadores'   => load_template('goleadores'),
      'promedios'    => load_template('promedios'),
      'nivel'        => load_template('libertadores/nivel'),
      'menu'         => load_template('menu')
    }
    @custom = {
      'deportes.futbol.libertadores' => {
        'estadisticas' => load_template('libertadores/estadisticas'),
        'fixture' => load_template('libertadores/fixture'),
        'nivel' => load_template('libertadores/nivel')
      },
      'deportes.futbol.mundial' => {
        'estadisticas' => load_template('mundial/estadisticas'),
        'marcador' => load_template('mundial/marcador'),
        'sidebar' => load_template('mundial/marcador-sidebar'),
        'grupo' => load_template('mundial/grupo'),
        'nivel' => load_template('mundial/nivel'),
        'ficha' => load_template('mundial/ficha')
      }
    }
  end

  def default name
    @default[name]
  end

  def custom name, canal
    @custom.each do |key, value|
      return value[name] if canal.start_with? key
    end
    nil
  end

  def load_template template
    Tilt::ERBTemplate.new(Rails.root.join('templates', "#{template}.erb").to_s)
  rescue
    $stderr.puts "Template '#{template}' no existe"
  end

end
