class Arbitro < ActiveRecord::Base

  def self.fetch! id
    where(id: id).first_or_create
  end

  def parse node
    self.id = node['id']
    self.nombre = node['nombre']
    self.nombre_corto = node['nc']
    self.pais = node['pais']
    self
  end

  def self.parse! xml
    fetch!(xml['id']).parse(xml).tap{|x| x.save}
  end

  def to_s
    nombre
  end

end
