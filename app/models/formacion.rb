class Formacion
  def initialize(integrantes)
    @integrantes = integrantes.sort_by{|x|x.orden.to_i}
  end

  def dt
    @integrantes.select(&:dt?).first
  end

  def titulares
    @integrantes.select &:titular?
  end

  def suplentes
    @integrantes.select &:suplente?
  end

end
