class Ficha < ActiveRecord::Base
  # belongs_to :fecha
  belongs_to :local, class_name: 'Equipo'
  belongs_to :visitante, class_name: 'Equipo'
  belongs_to :estadio
  belongs_to :arbitro
  belongs_to :partido, foreign_key: :id
  belongs_to :torneo
  has_many :incidencias
  has_many :integrantes

  def judadores_locales
    integrantes.equipo(local_id)
  end

  def judadores_visitantes
    integrantes.equipo(visitante_id)
  end

  def self.fetch! id
    where(id: id).first_or_create
  end

  def parse ficha, torneo
    self.id = ficha['id']
    self.dia = ficha['dia']
    self.fecha = ficha['fecha']
    self.torneo_id = torneo.id
    self.fn = ficha['fn']
    self.horario = ficha['horario']
    self.nombre_dia = ficha['nombreDia']
    self.nombre_nivel = ficha['nombrenivel']
    self.tipo = ficha['tipo']

    local = ficha.at_css('equipo[condicion=local]')
    visitante = ficha.at_css('equipo[condicion=visitante]')

    self.local_id = local['id']
    self.visitante_id = visitante['id']
    self.goles_local = local['goles']
    self.goles_visitante = visitante['goles']
    self.goles_penales_local = local['golesDefPenales']
    self.goles_penales_visitante = visitante['golesDefPenales']

    self.hora_inicio = ficha.at('horaInicio').text
    self.estado_evento = ficha.at('estadoEvento')['idestado']
    self.tiempo_evento = ficha.at('tiempoEvento').text
    self.minutos_evento = ficha.at('minutosEvento').text
    self.segundos_evento = ficha.at('segundosEvento').text
    self.descripcion_evento = ficha.at('descripcionEvento').text
    self.hora_estado_evento = ficha.at('horaEstadoEvento').text
    self.arbitro_id = get_id ficha, 'arbitro'
    self.estadio_id = ficha.at('estadioapodo')['id']
    self.espectadores = ficha.at('espectadores').text
    self.recaudacion = ficha.at('recaudacion').text

    [local, visitante].each do |equipo|
      equipo.search('integrante').each do |integrante|
        Integrante.parse! integrante, self.id, equipo['id']
      end
    end

    ficha.at('incidencias').search('incidencia').each do |incidencia|
      incidencias.parse! incidencia
    end

    self
  end

  def self.parse! node, torneo
    fetch!(node['id']).parse(node, torneo).tap{|x| x.save}
  end

  def goleadores_local
    incidencias.gol.where(equipo_id: local_id)
  end

  def goleadores_visitante
    incidencias.gol.where(equipo_id: visitante_id)
  end

  def titulares
    integrantes.select(&:titular?)
  end

private

  def get_id node, attribute
    node.at(attribute) && node.at(attribute)['id']
  end

end
