class Gamecast < Snippet
  def initialize(partido)
    @partido = partido
    super(nil, nil)
  end

  def url
    canal = "deportes.futbol.#{@partido.torneo.canal}.#{@partido.id}"
    Perfil::Application.config.gamecast_code % canal
  end

  def to_s
    "<iframe src='#{url}' width='100%' height='1185'></iframe>"
  end
end
