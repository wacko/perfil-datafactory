class Incidencia < ActiveRecord::Base

  belongs_to :ficha
  belongs_to :jugador
  belongs_to :equipo
  belongs_to :jugador_entra, class_name: 'Jugador'
  belongs_to :jugador_sale, class_name: 'Jugador'

  scope :gol, ->{ where(tipo: 'gol') }
  scope :cambio, ->{ where(tipo: 'cambio') }
  scope :amonestado, ->{ where(tipo: 'amonestado') }
  scope :roja, ->{ where(tipo: 'roja') }

  def self.fetch! id
    where(id: id).first_or_initialize
  end

  def parse node
    self.inciid = node['inciid']
    self.tipo = node['tipo']
    self.orden = node['orden']
    self.minuto = node.at('minuto').text
    self.tiempo = node.at('tiempo').text
    self.jugador_id = get_id node, 'jugador'
    self.jugador_entra_id = get_id node, 'jugadorentra'
    self.jugador_sale_id = get_id node, 'jugadorsale'
    self.equipo_id = get_id node, 'key'

    self
  end

  def self.parse! xml
    fetch!(xml['id']).parse(xml).tap{|x| x.save}
  end

private

  def get_id node, attribute
    node.at(attribute) && node.at(attribute)['id']
  end

end
