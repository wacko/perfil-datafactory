class PageGeneratorController < ApplicationController
  before_filter :check_if_admin!

  def ficha
    parser = Parsers::FichaParser.new
    parser.generate_html Ficha.find(params[:id]), "deportes.futbol.primeraa.ficha.#{params[:id]}"
    render nothing: true, status: 200
  end

  def goleadores
    torneo = Torneo.find params[:id]
    Parsers::GoleadoresParser.new.generate_html torneo.goleadores, "deportes.futbol.primeraa.goleadores"
    render nothing: true, status: 200
  end

  def posiciones
    torneo = Torneo.find params[:id]
    Parsers::PosicionesParser.new.generate_html torneo.posiciones, "deportes.futbol.primeraa.posiciones"
    render nothing: true, status: 200
  end

  def torneo
    torneo = Torneo.find params[:id]
    Parsers::TorneoParser.new.generate_html torneo, "deportes.futbol.primeraa.fixture"
    render nothing: true, status: 200
  end

  def fichas
    parser = Parsers::FichaParser.new
    Torneo.find(params[:id]).fichas.each do |ficha|
      parser.generate_html ficha, "deportes.futbol.primeraa.ficha"
    end
    render nothing: true, status: 200
  end

end
