class PagesController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::AssetTagHelper
  include ActionView::Helpers::UrlHelper

  def torneos
    torneo = Torneo.find(params[:id]).decorate
    @javascript = true
    render_template "estadisticas", torneo: torneo, canal: canal(torneo, 'estadisticas')
  end

  def posiciones
    torneo = Torneo.find(params[:id]).decorate
    posiciones = torneo.posiciones
    render_template 'posiciones', torneo: torneo, canal: canal(torneo, 'posiciones'), posiciones: posiciones
  end

  def fixture
    torneo = Torneo.find(params[:id]).decorate
    fechas = torneo.fechas
    fecha = torneo.fecha_actual
    @javascript = true
    render_template 'fixture', torneo: torneo, canal: canal(torneo, 'fixture'), fechas: fechas, fecha: fecha
  end

  def promedios
    torneo = Torneo.find(params[:id]).decorate
    promedios = PosicionesDecorator.decorate_collection torneo.posiciones.sort_by(&:promedio_descenso).reverse
    render_template 'promedios', torneo: torneo, canal: canal(torneo, 'promedios'), promedios: promedios
  end

  def goleadores
    torneo = Torneo.find(params[:id]).decorate
    goleadores = torneo.goleadores
    render_template 'goleadores', torneo: torneo, canal: canal(torneo, 'goleadores'), goleadores: goleadores
  end

  def marcador
    ficha = Ficha.find(params[:id]).decorate
    partido = ficha.partido
    fecha = partido.fecha
    torneo = fecha.torneo
    render_template 'marcador', torneo: torneo, canal: canal(torneo, 'ficha'), ficha: ficha, partido: partido, fecha: fecha
  end

  def marcador_sidebar
    ficha = Ficha.find(params[:id]).decorate
    partido = ficha.partido
    fecha = partido.fecha
    torneo = fecha.torneo
    render_template 'sidebar', torneo: torneo, canal: canal(torneo, 'ficha'), ficha: ficha, partido: partido, fecha: fecha
  end

  def ficha
    ficha = Ficha.find(params[:id]).decorate
    partido = ficha.partido
    fecha = partido.fecha
    torneo = fecha.torneo
    render_template 'ficha', {ficha: ficha.decorate, partido: partido.decorate, fecha: fecha.decorate, torneo: torneo.decorate, canal: canal(torneo, 'ficha')}
  end

  def fecha
    fecha = Fecha.find(params[:id])
    torneo = fecha.torneo
    @javascript = true
    render_template 'fecha', {fecha: fecha.decorate, torneo: torneo.decorate, canal: canal(torneo, 'fecha')}
  end

  def render_template template_name, params={}
    template = Template.for template_name, params[:canal]
    render text: template.render(self, params), layout: 'page'
  end

  def cdn_root
    Perfil::Application.config.cdn_server
  end

  def canal_tag canal
    canales = canal.split('.').map{|a| "df_#{a}"}.join ' '
    "<div class='#{canales}'>".html_safe
  end

private

  def canal torneo, template
    ['deportes', torneo.categoria.deporte, torneo.categoria.canal, template].join('.')
  end

end
