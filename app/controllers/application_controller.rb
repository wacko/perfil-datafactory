class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    Perfil::Application.config.backend_path + '/'
  end

  def after_sign_out_path_for(resource_or_scope)
    '/'
  end

  def check_if_admin!
    redirect_to admin_root_url unless current_admin_user.admin?
  end

end
