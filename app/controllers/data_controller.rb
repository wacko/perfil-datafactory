require 'net/telnet'

class DataController < ApplicationController
  before_filter :check_if_admin!

  def reload
    desde = params[:desde].gsub('-', '')
    params = {
      action: 'actualizar_canal',
      desde: desde,
      canal: 'deportes.futbol.primeraa'
    }

    status = 500
    daemon_server = Net::Telnet::new('Host' => 'localhost', 'Port' => 3004, 'Timeout' => 10, 'Prompt' => /[$%#>] \z/n)
    daemon_server.cmd(params.to_json) { |c| status = c }
    redirect_to admin_dashboard_url, flash: { success: 'Procesando...' }
  end
end
