ActiveAdmin.register Equipo do

  decorate_with EquipoDecorator
  config.sort_order = "nombre_asc"
  actions :index, :show, :edit, :update
  menu false
  filter :nombre
  filter :pais

  index do
    column :id
    column(:nombre){|x| x.link_con_escudo }
    column :nombre_asociacion
    column :pais
    column :to_slug
  end

  show title: :nombre do
    attributes_table do
      row :id
      row :nombre
      row :nombre_asociacion
      row :pais
      row(:slug){ |x| x.to_slug }
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs "Editar Equipo" do
      f.input :slug, placeholder: equipo.to_slug
    end
    f.actions
  end

  permit_params :slug

end
