ActiveAdmin.register AdminUser do

  menu :if => proc{ current_admin_user.admin? }

  permit_params :email, :password, :password_confirmation, :admin

  index do
    column :email
    column(:admin){|x| x.admin ? 'Si' : '' }
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    default_actions
  end

  filter :email

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :admin
    end
    f.actions
  end

  controller do
    before_filter :check_if_admin

    def check_if_admin
      redirect_to admin_root_url unless current_admin_user.admin?
    end

  end

end
