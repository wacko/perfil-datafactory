ActiveAdmin.register Goleador do

  belongs_to :torneo, class_name: 'Torneo'
  config.sort_order = 'goles_desc'
  actions :index, :show
  # decorate_with GoleadorDecorator
  config.filters = false

  index do
    column :nombre
    column(:equipo){|x| admin_link_to x.equipo}
    column :goles
    column :jugada
    column :cabeza
    column :tiro_libre
    column :penal
    # column :pais_id
  end

end
