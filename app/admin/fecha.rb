ActiveAdmin.register Fecha do

  belongs_to :torneo, class_name: 'Torneo', optional: true
  config.sort_order = 'numero_asc'
  actions :index, :show
  menu false
  decorate_with FechaDecorator
  config.filters = false

  index do
    column(:fecha){|x| x.nombre}
    column(:nivel){|x| x.nombre_nivel}
    column :estado
    column :desde
    column :hasta
    column('partidos'){|x| x.link_to_partidos}
    column('detalles'){|x| x.link_to_detalles}
  end

  show title: :descripcion do
    attributes_table do
      row :id
      row("Torneo"){|x| admin_link_to x.torneo}
      row :estado
      row :desde
      row :hasta
      row :numero
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

end
