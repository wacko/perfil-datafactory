ActiveAdmin.register Posiciones, as: 'Promedio' do

  belongs_to :torneo, class_name: 'Torneo'
  decorate_with PosicionesDecorator
  config.sort_order = 'promedio_descenso_desc'
  actions :index

  index do
    column(:nombre){|x| x.equipo.decorate.link_con_escudo }
    column('T1'){|x| x.puntos_anterior1 }
    column('T2'){|x| x.puntos_anterior2 }
    column('T3'){|x| x.puntos_actual }
    column(:puntos){|x| x.puntos_descenso }
    column(:partidos){|x| x.jugados_descenso }
    column(:promedio){|x| x.promedio_descenso }
  end

end
