ActiveAdmin.register Posiciones do

  belongs_to :torneo, class_name: 'Torneo'
  decorate_with PosicionesDecorator
  config.sort_order = 'puntos_desc'
  actions :index
  config.filters = false

  index do
    column(:nombre){|x| x.equipo.decorate.link_con_escudo }
    column :puntos
    column :jugados
    column :ganados
    column :empatados
    column :perdidos
    column('GF') { |x| x.goles_favor }
    column('GC') { |x| x.goles_contra }
    column('Diferencia') { |x| x.diferencia_de_gol }
    column('Promedio') { |x| x.promedio_descenso }
  end

end
