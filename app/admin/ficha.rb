ActiveAdmin.register Ficha do

  menu false
  decorate_with FichaDecorator

  filter :torneo
  filter :dia

  index do
    column :id
    column :local
    column :visitante
    column :goles_local
    column :goles_visitante
    column :dia
    column(:estado){|x| x.estado}
    column(:ficha){|x| x.ver_ficha}
  end

  show title: :titulo do
    columns do
      column do
        panel ficha.local_con_goles do
          table_for ficha.judadores_locales.titular do
            column("") { |x| x.camiseta }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
          table_for ficha.judadores_locales.suplente do
            column("") { |x| x.camiseta }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
          table_for ficha.judadores_locales.dt do
            column("") { "DT" }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
        end
      end
      column do
        panel ficha.visitante_con_goles do
          table_for ficha.judadores_visitantes.titular do
            column("") { |x| x.camiseta }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
          table_for ficha.judadores_visitantes.suplente do
            column("") { |x| x.camiseta }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
          table_for ficha.judadores_visitantes.dt do
            column("") { "DT" }
            column("") { |x| x.nombre }
            column("") { |x| x.tarjeta }
          end
        end
      end
    end

    panel 'Goles' do
      table_for ficha.incidencias.gol do
        column("") { |x| x.minuto }
        column("") { |x| x.tiempo }
        column("") { |x| x.jugador }
        column("") { |x| x.equipo }
      end
    end

    panel 'Amonestados' do
      table_for ficha.incidencias.amonestado do
        column("") { |x| x.minuto }
        column("") { |x| x.tiempo }
        column("") { |x| x.jugador }
        column("") { |x| x.equipo }
      end
    end

    panel 'Expulsados' do
      table_for ficha.incidencias.roja do
        column("") { |x| x.minuto }
        column("") { |x| x.tiempo }
        column("") { |x| x.jugador }
        column("") { |x| x.equipo }
      end
    end

    panel 'Cambios' do
      table_for ficha.incidencias.cambio do
        column("") { |x| x.minuto }
        column("") { |x| x.tiempo }
        column("") { |x| x.equipo }
        column("") { |x| x.jugador_sale }
        column("") { |x| x.jugador_entra }
      end
    end

    panel 'Codigo HTML' do
      content_tag(:pre, Snippet.new(resource, 'ficha'))
    end

  end
end
