ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, :if => proc{ current_admin_user.admin? }

  content title: 'Dashboard' do

    columns do
      column do
        if current_admin_user.admin?
          panel "Recargar datos" do
            render partial: "regenerar_html_form"
          end
        end
      end
    end
  end

end
