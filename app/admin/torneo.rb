ActiveAdmin.register Torneo do

  menu priority: 2

  decorate_with TorneoDecorator
  filter :categoria
  filter :campeonato
  filter :created_at

  index do
    column(:campeonato){|x| x.link_to_fechas x.campeonato }
    column :deporte
    column :categoria
    column :canal
    column(''){|x| x.link_to_estadisticas }
    column(''){|x| x.link_to_posiciones }
    column(''){|x| x.link_to_promedios }
    column(''){|x| x.link_to_goleadores }
    column(''){|x| x.link_to_detalles }
  end

  show title: :campeonato do
    attributes_table do
      row :id
      row :campeonato
      row :deporte
      row :categoria
      row :canal
      row :created_at
      row :updated_at
      row(:estadisticas){|x| x.link_to_estadisticas + snippet_icon(x, 'estadisticas') }
      row(:posiciones){|x| x.link_to_posiciones + snippet_icon(x, 'posiciones')}
      row(:fechas){|x| x.link_to_fechas + snippet_icon(x, 'fechas')}
      row(:goleadores){|x| x.link_to_goleadores + snippet_icon(x, 'goleadores')}
      row(:promedios){|x| x.link_to_promedios + snippet_icon(x, 'promedios')}
    end

    if current_admin_user.admin?
      panel "Regenerar HTML" do
        para link_to "Goleadores", generate_goleadores_path(torneo.id), remote: true
        para link_to "Posiciones", generate_posiciones_path(torneo.id), remote: true
        para link_to "Torneo", generate_torneo_path(torneo.id), remote: true
        para link_to "Fichas", generate_fichas_path(torneo.id), remote: true
      end
    end

    active_admin_comments
  end

end
