ActiveAdmin.register Partido do

  belongs_to :fecha, class_name: 'Fecha', optional: true
  actions :index, :show
  menu false
  decorate_with PartidoDecorator
  config.filters = false

  index do
    column :id
    column(:fecha){|x| admin_link_to x.fecha }
    column :estado
    column(:local){|x| x.link_escudo(x.local) + x.link_to_local_con_goles }
    column(:visitante){|x| x.link_escudo(x.visitante) + x.link_to_visitante_con_goles }
    column(:dia){|x| x.nombre_dia }
    column(:fecha){|x| x.fecha_partido }
    # column :arbitro
    # column :estadio
    column(:sidebar){|x| snippet_icon(x, 'sidebar')}
    column(:marcador){|x| snippet_icon(x, 'marcador')}
    column(:ficha){|x| snippet_icon(x, 'ficha')}
    column(:gamecast){|x| gamecast_icon(x)}
    column(''){|x| x.link_to_detalles}
  end

  show title: :titulo do
    attributes_table do
      row :id
      row(:torneo){|x| admin_link_to x.fecha.torneo }
      row(:fecha){|x| admin_link_to x.fecha }
      row :estado
      row(:local){|x| x.link_to_local_con_goles }
      row(:visitante){|x| x.link_to_visitante_con_goles }
      row(:ficha){|x| admin_link_to(x.ficha){'ver ficha'} if x.ficha }
      row :nombre_dia
      row :fecha_partido
      row :arbitro
      row(:estadio){|x| x.estadio.decorate.nombre_y_club }
      row :numero
      row :orden_agenda
      row :created_at
      row :updated_at
      row('Marcador'){|x| snippet(x, 'marcador') }
      row('Sidebar'){|x| snippet(x, 'sidebar') }
      row('Ficha'){|x| snippet(x, 'ficha') }
      row(:gamecast){|x| gamecast(x)}
    end

    panel "Regenerar HTML" do
      para link_to "Ficha", generate_ficha_path(partido.id), remote: true
    end

    active_admin_comments
  end

end
