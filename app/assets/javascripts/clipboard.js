$( document ).ready(function() {

  var client = new ZeroClipboard(document.getElementsByClassName("copy-button"), {
    moviePath: "/df/ZeroClipboard.swf"
  });

  client.on("load", function(client) {
    client.on("complete", function(client, args) {
      // `this` is the element that was clicked
      // this.style.display = "none";
      console.log("Copied text to clipboard: " + args.text);
    });
  });

});
