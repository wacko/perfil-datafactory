# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

Categoria.create [
  {id: 1,   deporte: "futbol", canal: "primeraa",      nombre: "Argentina - Primera A" },
  {id: 4,   deporte: "futbol", canal: "nacionalb",     nombre: "Argentina - PB Nacional" },
  {id: 5,   deporte: "futbol", canal: "espana",        nombre: "Liga de España" },
  {id: 6,   deporte: "futbol", canal: "italia",        nombre: "Liga de Italia" },
  {id: 7,   deporte: "futbol", canal: "libertadores",  nombre: "Copa Libertadores" },
  {id: 24,  deporte: "futbol", canal: "amistoso",      nombre: "Amistosos" },
  {id: 30,  deporte: "futbol", canal: "premierleague", nombre: "Premier League" },
  {id: 32,  deporte: "futbol", canal: "champions",     nombre: "UEFA Champions League" },
  {id: 34,  deporte: "futbol", canal: "uefa",          nombre: "UEFA Europa League" },
  {id: 38,  deporte: "futbol", canal: "primerab",      nombre: "Argentina - Primera B" },
  {id: 39,  deporte: "futbol", canal: "primerac",      nombre: "Argentina - Primera C" },
  {id: 45,  deporte: "futbol", canal: "primerad",      nombre: "Argentina - Primera D" },
  {id: 60,  deporte: "futbol", canal: "copadesafio",   nombre: "Torneos de Verano" },
  {id: 65,  deporte: "tenis",  canal: "atpbsas",       nombre: "ATP Buenos Aires" },
  {id: 70,  deporte: "tenis",  canal: "indianwells",   nombre: "Masters 1000 INDIAN WELLS" },
  {id: 163, deporte: "futbol", canal: "copaargentina", nombre: "Argentina - Copa Argentina" }
]
