# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140618173053) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "arbitros", force: true do |t|
    t.string   "nombre"
    t.string   "nombre_corto"
    t.string   "pais"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categoria", force: true do |t|
    t.string   "deporte"
    t.string   "canal"
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "equipos", force: true do |t|
    t.string   "nombre"
    t.string   "nombre_asociacion"
    t.string   "pais"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "estadios", force: true do |t|
    t.string   "club"
    t.string   "ciudad"
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fechas", force: true do |t|
    t.integer  "torneo_id"
    t.string   "estado"
    t.date     "desde"
    t.date     "hasta"
    t.integer  "numero"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "nivel"
    t.string   "nombre_nivel"
    t.string   "nombre"
    t.integer  "orden"
  end

  add_index "fechas", ["torneo_id"], name: "index_fechas_on_torneo_id", using: :btree

  create_table "fichas", force: true do |t|
    t.integer  "local_id"
    t.integer  "visitante_id"
    t.integer  "goles_local"
    t.integer  "goles_penales_local"
    t.integer  "goles_visitante"
    t.integer  "goles_penales_visitante"
    t.date     "dia"
    t.string   "fecha"
    t.integer  "fn"
    t.string   "horario"
    t.string   "nombre_dia"
    t.string   "nombre_nivel"
    t.string   "tipo"
    t.string   "hora_inicio"
    t.integer  "estado_evento"
    t.integer  "tiempo_evento"
    t.integer  "minutos_evento"
    t.integer  "segundos_evento"
    t.string   "descripcion_evento"
    t.string   "hora_estado_evento"
    t.integer  "arbitro_id"
    t.integer  "estadio_id"
    t.integer  "espectadores"
    t.integer  "recaudacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fecha_id"
    t.integer  "torneo_id"
  end

  add_index "fichas", ["arbitro_id"], name: "index_fichas_on_arbitro_id", using: :btree
  add_index "fichas", ["estadio_id"], name: "index_fichas_on_estadio_id", using: :btree
  add_index "fichas", ["fecha_id"], name: "index_fichas_on_fecha_id", using: :btree
  add_index "fichas", ["local_id"], name: "index_fichas_on_local_id", using: :btree
  add_index "fichas", ["torneo_id"], name: "index_fichas_on_torneo_id", using: :btree
  add_index "fichas", ["visitante_id"], name: "index_fichas_on_visitante_id", using: :btree

  create_table "goleadores", force: true do |t|
    t.integer  "torneo_id"
    t.date     "fecha"
    t.integer  "persona_id"
    t.string   "nombre"
    t.integer  "equipo_id"
    t.integer  "goles"
    t.integer  "jugada"
    t.integer  "cabeza"
    t.integer  "tiro_libre"
    t.integer  "penal"
    t.integer  "pais_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "goleadores", ["equipo_id"], name: "index_goleadores_on_equipo_id", using: :btree
  add_index "goleadores", ["torneo_id"], name: "index_goleadores_on_torneo_id", using: :btree

  create_table "incidencias", force: true do |t|
    t.integer  "ficha_id"
    t.string   "inciid"
    t.string   "tipo"
    t.string   "orden"
    t.string   "minuto"
    t.string   "tiempo"
    t.integer  "jugador_id"
    t.integer  "jugador_entra_id"
    t.integer  "jugador_sale_id"
    t.integer  "equipo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "incidencias", ["equipo_id"], name: "index_incidencias_on_equipo_id", using: :btree
  add_index "incidencias", ["ficha_id"], name: "index_incidencias_on_ficha_id", using: :btree
  add_index "incidencias", ["jugador_entra_id"], name: "index_incidencias_on_jugador_entra_id", using: :btree
  add_index "incidencias", ["jugador_id"], name: "index_incidencias_on_jugador_id", using: :btree
  add_index "incidencias", ["jugador_sale_id"], name: "index_incidencias_on_jugador_sale_id", using: :btree

  create_table "integrantes", force: true do |t|
    t.integer  "jugador_id"
    t.integer  "ficha_id"
    t.integer  "equipo_id"
    t.integer  "camiseta"
    t.integer  "goles"
    t.integer  "amarilla"
    t.integer  "roja"
    t.integer  "idrol"
    t.integer  "orden"
    t.string   "rol"
    t.string   "tipo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "integrantes", ["equipo_id"], name: "index_integrantes_on_equipo_id", using: :btree
  add_index "integrantes", ["ficha_id"], name: "index_integrantes_on_ficha_id", using: :btree
  add_index "integrantes", ["jugador_id"], name: "index_integrantes_on_jugador_id", using: :btree

  create_table "jugadores", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "partidos", force: true do |t|
    t.integer  "fecha_id"
    t.string   "estado"
    t.integer  "local_id"
    t.integer  "visitante_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "goles_local",           default: 0
    t.integer  "goles_visitante",       default: 0
    t.datetime "fecha_partido"
    t.integer  "estadio_id"
    t.integer  "arbitro_id"
    t.string   "nombre_dia"
    t.integer  "numero"
    t.integer  "orden_agenda"
    t.integer  "torneo_id"
    t.integer  "ganador_id"
    t.string   "medios"
    t.string   "local_descripcion"
    t.string   "visitante_descripcion"
  end

  add_index "partidos", ["arbitro_id"], name: "index_partidos_on_arbitro_id", using: :btree
  add_index "partidos", ["estadio_id"], name: "index_partidos_on_estadio_id", using: :btree
  add_index "partidos", ["fecha_id"], name: "index_partidos_on_fecha_id", using: :btree
  add_index "partidos", ["ganador_id"], name: "index_partidos_on_ganador_id", using: :btree
  add_index "partidos", ["torneo_id"], name: "index_partidos_on_torneo_id", using: :btree

  create_table "posiciones", force: true do |t|
    t.integer  "torneo_id"
    t.integer  "equipo_id"
    t.string   "nombre"
    t.integer  "puntos"
    t.integer  "jugados"
    t.integer  "jugados_local"
    t.integer  "jugados_visitante"
    t.integer  "ganados"
    t.integer  "empatados"
    t.integer  "perdidos"
    t.integer  "ganados_local"
    t.integer  "empatados_local"
    t.integer  "perdidos_local"
    t.integer  "ganados_visitante"
    t.integer  "empatados_visitante"
    t.integer  "perdidos_visitante"
    t.integer  "goles_favor_local"
    t.integer  "goles_contra_local"
    t.integer  "goles_favor_visitante"
    t.integer  "goles_contra_visitante"
    t.integer  "goles_favor"
    t.integer  "goles_contra"
    t.integer  "diferencia_de_gol"
    t.integer  "puntos_local"
    t.integer  "puntos_visitante"
    t.integer  "puntos_anterior1"
    t.integer  "jugados_anterior1"
    t.integer  "puntos_anterior2"
    t.integer  "jugados_anterior2"
    t.integer  "puntos_actual"
    t.integer  "diferencia_de_gol_actual"
    t.integer  "jugados_actual"
    t.integer  "puntos_descenso"
    t.integer  "jugados_descenso"
    t.decimal  "promedio_descenso",                precision: 4, scale: 3
    t.decimal  "promedio_descenso2",               precision: 4, scale: 3
    t.integer  "amarillas"
    t.integer  "rojas"
    t.integer  "roja_doble_amonestacion"
    t.integer  "faltas_penal"
    t.integer  "mano_penal"
    t.integer  "faltas_cometidas"
    t.integer  "faltas_recibidas"
    t.integer  "faltas_penal_recibidas"
    t.integer  "nivel"
    t.string   "nivel_descripcion"
    t.integer  "orden"
    t.string   "orden_descripcion"
    t.string   "descripcion_tribunal_disciplina"
    t.string   "partidos_def_tribunal_disciplina"
    t.integer  "racha"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "zona"
  end

  add_index "posiciones", ["equipo_id"], name: "index_posiciones_on_equipo_id", using: :btree
  add_index "posiciones", ["torneo_id"], name: "index_posiciones_on_torneo_id", using: :btree

  create_table "torneos", force: true do |t|
    t.string   "campeonato"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "categoria_id"
  end

  add_index "torneos", ["categoria_id"], name: "index_torneos_on_categoria_id", using: :btree

end
