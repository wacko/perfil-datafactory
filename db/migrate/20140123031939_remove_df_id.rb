class RemoveDfId < ActiveRecord::Migration
  def change
    remove_column :torneos,  :df_id, :string
    remove_column :partidos, :df_id, :string
    remove_column :equipos,  :df_id, :string
    remove_column :fechas,   :df_id, :string
    remove_column :posiciones, :equipo_df_id, :string
  end
end
