class AddGanadorToPartido < ActiveRecord::Migration
  def change
    add_reference :partidos, :ganador, index: true
  end
end
