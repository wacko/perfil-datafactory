class CreateTorneos < ActiveRecord::Migration
  def change
    create_table :torneos do |t|
      t.integer :df_id
      t.string :deporte
      t.string :categoria
      t.string :canal
      t.string :campeonato

      t.timestamps
    end
  end
end
