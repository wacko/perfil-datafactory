class CreateEquipos < ActiveRecord::Migration
  def change
    create_table :equipos do |t|
      t.integer :df_id
      t.string :nombre
      t.string :nombre_asociacion
      t.string :pais

      t.timestamps
    end
  end
end
