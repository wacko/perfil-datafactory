class CreateFichas < ActiveRecord::Migration
  def change
    create_table :fichas do |t|
      t.references :local, index: true
      t.references :visitante, index: true
      t.integer :goles_local
      t.integer :goles_penales_local
      t.integer :goles_visitante
      t.integer :goles_penales_visitante
      t.date :dia
      t.string :fecha
      t.integer :fn
      t.string :horario
      t.string :nombre_dia
      t.string :nombre_nivel
      t.string :tipo
      t.string :hora_inicio
      t.integer :estado_evento
      t.integer :tiempo_evento
      t.integer :minutos_evento
      t.integer :segundos_evento
      t.string :descripcion_evento
      t.string :hora_estado_evento
      t.references :arbitro, index: true
      t.references :estadio, index: true
      t.integer :espectadores
      t.integer :recaudacion

      t.timestamps
    end
  end
end
