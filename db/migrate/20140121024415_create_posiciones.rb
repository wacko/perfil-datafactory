class CreatePosiciones < ActiveRecord::Migration
  def change
    create_table :posiciones do |t|
      t.references :torneo, index: true
      t.references :equipo, index: true
      t.integer :equipo_df_id, index: true
      t.string :nombre
      t.integer :puntos
      t.integer :jugados
      t.integer :jugados_local
      t.integer :jugados_visitante
      t.integer :ganados
      t.integer :empatados
      t.integer :perdidos
      t.integer :ganados_local
      t.integer :empatados_local
      t.integer :perdidos_local
      t.integer :ganados_visitante
      t.integer :empatados_visitante
      t.integer :perdidos_visitante
      t.integer :goles_favor_local
      t.integer :goles_contra_local
      t.integer :goles_favor_visitante
      t.integer :goles_contra_visitante
      t.integer :goles_favor
      t.integer :goles_contra
      t.integer :diferencia_de_gol
      t.integer :puntos_local
      t.integer :puntos_visitante
      t.integer :puntos_anterior1
      t.integer :jugados_anterior1
      t.integer :puntos_anterior2
      t.integer :jugados_anterior2
      t.integer :puntos_actual
      t.integer :diferencia_de_gol_actual
      t.integer :jugados_actual
      t.integer :puntos_descenso
      t.integer :jugados_descenso
      t.decimal :promedio_descenso, precision: 4, scale: 3
      t.decimal :promedio_descenso2, precision: 4, scale: 3
      t.integer :amarillas
      t.integer :rojas
      t.integer :roja_doble_amonestacion
      t.integer :faltas_penal
      t.integer :mano_penal
      t.integer :faltas_cometidas
      t.integer :faltas_recibidas
      t.integer :faltas_penal_recibidas
      t.integer :nivel
      t.string :nivel_descripcion
      t.integer :orden
      t.string :orden_descripcion
      t.string :descripcion_tribunal_disciplina
      t.string :partidos_def_tribunal_disciplina
      t.integer :racha

      t.timestamps
    end
  end
end
