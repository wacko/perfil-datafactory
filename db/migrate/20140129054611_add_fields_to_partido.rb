class AddFieldsToPartido < ActiveRecord::Migration
  def change
    remove_column :partidos, :arbitro, :string
    remove_column :partidos, :club_estadio, :string
    add_reference :partidos, :estadio, index: true
    add_reference :partidos, :arbitro, index: true
    add_column :partidos, :nombre_dia, :string
    add_column :partidos, :numero, :integer
    add_column :partidos, :orden_agenda, :integer
  end
end
