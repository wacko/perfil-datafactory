class AddGolesToPartido < ActiveRecord::Migration
  def change
    add_column :partidos, :goles_local, :integer
    add_column :partidos, :goles_visitante, :integer
    add_column :partidos, :fecha_partido, :datetime
    remove_column :partidos, :resultado, :integer
    remove_column :partidos, :fecha, :date
  end
end
