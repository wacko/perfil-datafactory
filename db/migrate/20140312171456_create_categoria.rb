class CreateCategoria < ActiveRecord::Migration
  def change
    create_table :categoria do |t|
      t.string :deporte
      t.string :canal
      t.string :nombre

      t.timestamps
    end

    remove_column :torneos, :categoria, :string
    remove_column :torneos, :canal, :string
    remove_column :torneos, :deporte, :string
    add_reference :torneos, :categoria, index: true
  end
end
