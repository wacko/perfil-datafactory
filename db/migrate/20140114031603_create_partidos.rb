class CreatePartidos < ActiveRecord::Migration
  def change
    create_table :partidos do |t|
      t.integer :df_id
      t.references :fecha, index: true
      t.string :club_estadio
      t.date :fecha
      t.string :estado
      t.references :local
      t.references :visitante
      t.string :resultado
      t.string :arbitro

      t.timestamps
    end
  end
end
