class CreateIncidencias < ActiveRecord::Migration
  def change
    create_table :incidencias do |t|
      t.references :ficha, index: true
      t.string :inciid
      t.string :tipo
      t.string :orden
      t.string :minuto
      t.string :tiempo
      t.references :jugador, index: true
      t.references :jugador_entra, index: true
      t.references :jugador_sale, index: true
      t.references :equipo, index: true

      t.timestamps
    end
  end
end
