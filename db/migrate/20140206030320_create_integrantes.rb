class CreateIntegrantes < ActiveRecord::Migration
  def change
    create_table :integrantes do |t|
      t.references :jugador, index: true
      t.references :ficha, index: true
      t.references :equipo, index: true
      t.integer :camiseta
      t.integer :goles
      t.integer :amarilla
      t.integer :roja
      t.integer :idrol
      t.integer :orden
      t.string :rol
      t.string :tipo

      t.timestamps
    end
  end
end
