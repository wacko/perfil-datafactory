class AddNivelToFecha < ActiveRecord::Migration
  def change
    add_column :fechas, :nivel, :integer
    add_column :fechas, :nombre_nivel, :string
    add_column :fechas, :nombre, :string
  end
end
