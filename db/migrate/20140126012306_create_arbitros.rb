class CreateArbitros < ActiveRecord::Migration
  def change
    create_table :arbitros do |t|
      t.string :nombre
      t.string :nombre_corto
      t.string :pais

      t.timestamps
    end
  end
end
