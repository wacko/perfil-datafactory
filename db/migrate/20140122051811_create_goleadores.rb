class CreateGoleadores < ActiveRecord::Migration
  def change
    create_table :goleadores do |t|
      t.references :torneo, index: true
      t.date :fecha
      t.integer :persona_id
      t.string :nombre
      t.references :equipo, index: true
      t.integer :goles
      t.integer :jugada
      t.integer :cabeza
      t.integer :tiro_libre
      t.integer :penal
      t.integer :pais_id

      t.timestamps
    end
  end
end
