class SetGolesDefault < ActiveRecord::Migration
  def up
    change_column :partidos, :goles_local, :integer, default: 0
    change_column :partidos, :goles_visitante, :integer, default: 0
    Partido.where(goles_local: nil).update_all(goles_local: 0)
    Partido.where(goles_visitante: nil).update_all(goles_visitante: 0)
  end

  def down
    change_column :partidos, :goles_local, :integer
    change_column :partidos, :goles_visitante, :integer
  end
end
