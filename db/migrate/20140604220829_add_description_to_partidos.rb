class AddDescriptionToPartidos < ActiveRecord::Migration
  def change
    add_column :partidos, :local_descripcion, :string
    add_column :partidos, :visitante_descripcion, :string
  end
end
