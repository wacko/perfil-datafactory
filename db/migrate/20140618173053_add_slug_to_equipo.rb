class AddSlugToEquipo < ActiveRecord::Migration
  def change
    add_column :equipos, :slug, :string
  end
end
