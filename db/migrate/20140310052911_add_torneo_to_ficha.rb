class AddTorneoToFicha < ActiveRecord::Migration
  def up
    add_reference :fichas, :torneo, index: true
    add_reference :partidos, :torneo, index: true

    sql_partidos = "UPDATE partidos SET torneo_id = (SELECT torneo_id FROM fechas WHERE partidos.fecha_id = fechas.id)"
    sql_fichas   = "UPDATE fichas INNER JOIN partidos on fichas.id = partidos.id SET fichas.torneo_id = partidos.torneo_id"
    ActiveRecord::Base.connection.execute sql_partidos
    ActiveRecord::Base.connection.execute sql_fichas
  end

  def down
    remove_reference :fichas, :torneo, index: true
    remove_reference :partidos, :torneo, index: true
  end
end
