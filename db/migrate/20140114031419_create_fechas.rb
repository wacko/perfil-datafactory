class CreateFechas < ActiveRecord::Migration
  def change
    create_table :fechas do |t|
      t.integer :df_id
      t.references :torneo, index: true
      t.string :estado
      t.date :desde
      t.date :hasta
      t.integer :numero

      t.timestamps
    end
  end
end
