class CreateEstadios < ActiveRecord::Migration
  def change
    create_table :estadios do |t|
      t.string :club
      t.string :ciudad
      t.string :nombre

      t.timestamps
    end
  end
end
