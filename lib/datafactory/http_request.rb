require 'em-http'
require 'nokogiri'
require 'dotenv'
Dotenv.load

module Datafactory
  class HttpRequest

    include EM::Deferrable

    HOST = ENV['DATAFACTORY_HOST']
    URI  = '/clientes/xml/index.php'

    # Se inicializa con un timestamp o un canal
    def initialize(params={})
      @params = params
    end

    def url
      HOST + URI
    end

    def connection_params
      return {} if ENV['HTTP_PROXY_HOST'].nil? and ENV['HTTP_PROXY_PORT'].nil?
      {
        proxy: {
          host: ENV['HTTP_PROXY_HOST'],
          port: ENV['HTTP_PROXY_PORT']
        }
      }
    end

    def run
      Datafactory.logger.info "GET #{@params.to_query}"
      request = EM::HttpRequest.new(url, connection_params).get(query: @params)
      request.callback {
        begin
          if request.response_header.status == 200
            succeed response_to_xml(request.response)
          else
            fail "ERROR (HTTP #{request.response_header.status}) al leer el canal"
          end
        rescue Exception => e
          fail "ERROR al procesar el canal \n #{e} \n #{e.backtrace.join("\n")}"
        end
      }
      request.errback {
        fail "ERROR de conexion"
      }
      self
    end

    def response_to_xml response_text
      Nokogiri::XML(response_text, nil, 'ISO-8859-1')
    end

    def self.actualizar ultima_actualizacion, notifier
      HttpRequest.new(ultima_actualizacion)
        .run
        .callback { |canales|
          canales.
            search('canal').
            map(&:text).
            uniq.
            each { |c| notifier.notify c }
        }
        .errback { |error| Datafactory.logger.error error }
    end

  end
end
