module Datafactory
  class ChannelsNotifier

    Notifier = Struct.new :filter, :observer
    def initialize
      @observers = []
    end

    def add_observer filter, observer
      @observers << Notifier.new(filter, observer)
    end

    def call canal
      @observers.each do |notifier|
        if notifier.filter =~ canal
          notifier.observer.call(canal)
        end
      end
    end

    alias_method :notify, :call

  end
end
