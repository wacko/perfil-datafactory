require_relative 'channels_notifier'

module Datafactory
  class Modulos

    def notifier
      # Algunos torneos tienen formato liga (ej: Primera A) y otros formato de grupos + eliminatorios (Ej: Libertadores/Mundial)

      parsers_para_ligas = Datafactory::ChannelsNotifier.new
      parsers_para_ligas.add_observer /fixture/,     parse_callback(Parsers::TorneoParser.new)
      parsers_para_ligas.add_observer /fecha/,       parse_callback(Parsers::TorneoParser.new)
      parsers_para_ligas.add_observer /ficha/,       parse_callback(Parsers::FichaParser.new)
      parsers_para_ligas.add_observer /goleadores$/, parse_callback(Parsers::GoleadoresParser.new)
      parsers_para_ligas.add_observer /posiciones/,  parse_callback(Parsers::PosicionesParser.new)

      libertadores = Datafactory::ChannelsNotifier.new
      libertadores.add_observer /fixture$/,      parse_callback(Parsers::TorneoPorGruposParser.new)
      libertadores.add_observer /ficha/,         parse_callback(Parsers::FichaParser.new)
      libertadores.add_observer /posiciones.2$/, parse_callback(Parsers::PosicionesParser.new)

      mundial = Datafactory::ChannelsNotifier.new
      mundial.add_observer /fixture$/,      parse_callback(Parsers::MundialParser.new)
      mundial.add_observer /ficha/,         parse_callback(Parsers::FichaParser.new)
      mundial.add_observer /posiciones$/,   parse_callback(Parsers::PosicionesParser.new)
      mundial.add_observer /goleadores$/,   parse_callback(Parsers::GoleadoresParser.new)

      ligas = [
        /deportes.futbol.primeraa/,
        /deportes.futbol.primerab/,
        /deportes.futbol.nacionalb/
      ]

      notifier = ChannelsNotifier.new
      ligas.each  { |torneo| notifier.add_observer torneo, parsers_para_ligas }
      notifier.add_observer(/deportes.futbol.libertadores/, libertadores)
      notifier.add_observer(/deportes.futbol.mundial/, mundial)

      notifier
    end

    def parse_callback parser
      lambda{ |canal|
        HttpRequest.new(canal: canal).run.
          callback { |xml|
            begin
              Datafactory.logger.info "PARSE #{canal}"
              obj = parser.parse(xml)
              parser.generate_html obj, canal
            rescue Exception => e
              Datafactory.logger.error e
            end
          }
      }
    end

  end
end
