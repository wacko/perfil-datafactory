require 'eventmachine'
require_relative 'http_request'
require_relative 'daemonizable'
require_relative 'telnet'
require_relative 'modulos'

module Datafactory

  class Server
    include Daemonizable

    def run
      EM.run{
        Signal.trap('INT') { stop }
        Signal.trap('TERM'){ stop }

        notifier = Modulos.new.notifier

        EM.add_timer(1) do
          Datafactory.logger.info 'The EventMachine is runing'
        end

        EM.add_periodic_timer(60) do
          begin
            # Offset de 10' para evitar problemas de sincronización con Datafactory
            dia, hora = (Time.now - 10*60).to_s.split
            ultima_actualizacion = {desde: dia.gsub('-', ''), hora: hora}

            HttpRequest.actualizar ultima_actualizacion, notifier
          rescue Exception => e
            Datafactory.logger.error e
          end
        end

        # Levanta un server telnet para poder mandar instrucciones a la EventMachine
        EM::start_server('localhost', 3004, Telnet) do |telnet|
          telnet.notifier = notifier
        end
      }
    end

  end
end
