module Datafactory
  class Telnet < EM::Connection

    attr_accessor :notifier

    def receive_data(data)
      params = JSON.parse data
      if params['action'] == 'actualizar_canal'
        actualizar_canal(params)
        send_data '200'
        close_connection_after_writing
      else
        close_with_error params
      end
    rescue Exception => e
      Datafactory.logger.info e
      close_with_error params
    end

    def actualizar_canal params
      desde = params['desde']
      Datafactory.logger.info "Comando: Actualizar HttpRequest"
      HttpRequest.actualizar({desde: desde}, notifier)
    end

    def close_with_error params
      Datafactory.logger.info "Comando no reconocido: #{params}"
      send_data '400'
      close_connection_after_writing
    end

  end
end
