module Datafactory
  module Daemonizable

    def pid_file
      @pid_file ||= Rails.root.join('tmp', 'pids', *self.class.name.underscore.gsub('/', '_') + ".pid")
    end

    def get_pid
      if File.exists?(pid_file)
        file = File.new(pid_file, "r")
        pid = file.read
        file.close

        pid
      else
        0
      end
    end

    def start
      pid = get_pid
      if pid != 0
        Datafactory.logger.warn "Daemon is already running (PID: #{pid})"
        exit -1
      end

      pid = fork {
        run
      }
      begin
        file = File.new(pid_file, "w")
        file.write(pid)
        file.close
        Process.detach(pid)
        Datafactory.logger.info "Daemon started (PID: #{pid})"
      rescue => exc
        Process.kill('TERM', pid)
        Datafactory.logger.error "Cannot start daemon: #{exc.message}"
      end
    end

    def stop
      pid = get_pid
      begin
        EM.stop
      rescue
      end

      if pid != 0
        Process.kill('HUP', pid.to_i)
        File.delete(pid_file)
        Datafactory.logger.info "Daemon stopped"
      else
        Datafactory.logger.warn "Daemon is not running"
        exit -1
      end
    end

  end
end
