module Datafactory

  mattr_accessor :logger
  self.logger = Logger.new(Rails.root.join('log', 'daemon.log')).tap do |log|
    log.progname = 'daemon'
  end

end
