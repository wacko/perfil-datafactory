class CustomAuthenticationFailure < Devise::FailureApp

protected

  def redirect_url
    if Rails.env == 'production'
      Perfil::Application.config.backend_path + new_admin_user_session_path
    else
      new_admin_user_session_path
    end
  end

end
