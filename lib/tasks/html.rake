require 'net/http'

# Uso:
# rake datafactory:html:ficha -- 101 102 103

namespace :datafactory do
  namespace :html do |args|

    desc "Regenera los html de ficha"
    task ficha: :environment do
      parser = Parsers::FichaParser.new
      ARGV.shift
      Ficha.find(ARGV).each { |ficha| parser.generate_html ficha, "deportes.futbol.primeraa.ficha.#{ficha.id}"}
      exit # avoids triggering params as rake tasks
    end

    desc "Regenera los html de goleadores"
    task goleadores: :environment do
      torneo = Torneo.find ARGV.last
      Parsers::GoleadoresParser.new.generate_html torneo.goleadores, "deportes.futbol.primeraa.goleadores"
      exit # avoids triggering params as rake tasks
    end

    desc "Regenera los html de posiciones"
    task posiciones: :environment do
      torneo = Torneo.find ARGV.last
      Parsers::PosicionesParser.new.generate_html torneo.posiciones, "deportes.futbol.primeraa.posiciones"
      exit # avoids triggering params as rake tasks
    end

    desc "Regenera los html de torneo/fecha"
    task torneo: :environment do
      torneo = Torneo.find ARGV.last
      Parsers::TorneoParser.new.generate_html torneo, "deportes.futbol.primeraa.fixture"
      exit # avoids triggering params as rake tasks
    end

    desc "Regenera el html con la lista de torneos"
    task menu: :environment do
      Parsers::MenuParser.new.generate_html nil, nil
    end

  end
end
