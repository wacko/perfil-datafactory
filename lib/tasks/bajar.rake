require "net/http"

namespace :datafactory do
  namespace :bajar do
    def http_request url
      uri = URI.parse(url)
      http = Net::HTTP::Proxy(ENV['HTTP_PROXY_HOST'], ENV['HTTP_PROXY_PORT']).new(uri.host, uri.port)
      response = http.request(Net::HTTP::Get.new(uri.request_uri))
      Nokogiri::XML(response.body, nil, 'ISO-8859-1')
    end

    def load_canal canal, parser_class
      url = "http://www.datafactory.ws/clientes/xml/index.php?canal=#{canal}"
      parser = parser_class.new
      obj = parser.parse http_request(url)
      parser.generate_html obj, canal
    end

    task torneo: :environment do
      load_canal 'deportes.futbol.primeraa.fixture', Parsers::TorneoParser
      puts "Torneo cargado"
    end

    task posiciones: :environment do
      load_canal 'deportes.futbol.primeraa.posiciones', Parsers::PosicionesParser
      puts "Posiciones cargadas"
    end

    task goleadores: :environment do
      load_canal 'deportes.futbol.primeraa.goleadores', Parsers::GoleadoresParser
      puts "Goleadores cargados"
    end

    task ficha: :environment do
      ARGV.shift
      ARGV.each do |id|
        puts "bajando deportes.futbol.primeraa.ficha.#{id} ..."
        ficha = load_canal "deportes.futbol.primeraa.ficha.#{id}", Parsers::FichaParser
        puts "Ficha #{id} cargada"
      end
      exit # avoids triggering params as rake tasks
    end

    task fecha: :environment do
      ARGV.shift
      ARGV.each do |id|
        puts "bajando deportes.futbol.primeraa.fecha.#{id} ..."
        fecha = load_canal "deportes.futbol.primeraa.fecha.#{id}", Parsers::TorneoParser
        puts "Fecha #{id} cargada"
      end
      exit # avoids triggering params as rake tasks
    end

  end
end
