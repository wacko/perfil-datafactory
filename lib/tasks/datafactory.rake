namespace :datafactory do
  namespace :daemon do

    desc 'Inicia el Daemon'
    task start: :environment do
      Datafactory::Server.new.start
    end

    desc 'Termina el Daemon'
    task stop: :environment do
      Datafactory::Server.new.stop
    end

    task delete_pid: :environment do
      pid = Datafactory::Server.new.pid_file
      File.delete(pid) if File.exists? pid
    end

    desc 'Termina el proceso del Daemon e inicia uno nuevo'
    task reset: [:environment, :stop, :delete_pid, :start]

  end
end
